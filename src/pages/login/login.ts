import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SessionsProvider } from '../../providers/sessions/sessions';
import { TabsPage } from '../tabs/tabs';
import { MyApp } from '../../app/app.component';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {

  username: string;
  password: string;
  logado: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public sessions: SessionsProvider) {

  }

  login(){
    if(this.sessions.login(this.username,this.password)){
      this.navCtrl.setRoot(TabsPage);
    }else{
      console.log("Usuário e senha incorretos");
      }
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
