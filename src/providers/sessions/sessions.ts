import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';


/*
  Generated class for the SessionsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SessionsProvider {
  [x: string]: any;

  username = "teste@email.com";
  password = "123456";

  constructor() {

  }   
  

  login (username: string,password: string): boolean {
    if (username === this.username && password === this.password){
      localStorage.setItem("logado", "true");
      return true;
    }else{
      return false;
    }
  }

  logout():void{
    localStorage.removeItem("logado");
  
  }
}
